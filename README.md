# HomeMed
Tugas Kelompok B09 - Mata Kuliah Pemrograman Berbasis Platform
Fakultas Ilmu Komputer UI, Semester Ganjil 2021/2022

---

## Anggota Kelompok
1. Aurora Putri Kumala Bakti - 2006597323	
2. Brasel Isnaen Fachturrahman - 2006597374	
3. Christopher Moses Nathanael - 2006597361	
4. Dimitri Theofilus Wynardo N - 2006597670	
5. Galan Gahara Triguna Toding - 2006597121	
6. Muhammad Arya Adirianto - 2006597494	
7. Nathasya Shalsabilla Putri - 2006597531	

## Deskripsi Aplikasi
Di tengah hiruknya masyarakat Indonesia akibat **Pandemi Covid-19** yang semakin pesat dan membludak, muncul permasalahan baru yaitu susahnya mencari obat dan alat kesehatan. Masyarakat sudah susah untuk beraktivitas karena keterbatasan protokol kesehatan yang ditetapkan ditambah harus susah payah juga dalam mencari obat dan alat kesehatan baik untuk penanganan penyakit maupun persediaan di rumah. Akhir-akhir ini, masyarakat Indonesia juga sedang digemparkan dengan susahnya mencari oxygen di sekitarnya. Banyak sekali hal menjadi keos selama pandemi ini. Masyarakat tak terhindarkan dari godaan untuk membeli alat kesehatan yang termurah dan tercepat aksesnya yang mereka bisa dapatkan. Muncullah permasalahan baru seperti contoh penipuan terhadap tabung oxygen yang diisi dengan nitrogen dan penipuan terhadap lapisan layer masker. Untuk itu, kami hadir bertujuan meminimalisir kekacauan yang terjadi dengan cara membuat sebuah aplikasi bagi masyarakat Indonesia berbasis website.

**Aplikasi HomeMed** hadir sebagai aplikasi untuk membantu masyarakat Indonesia dalam membeli alat kesehatan atau medis yang aman dan terpercaya. Aplikasi ini bekerja sama secara langsung dengan Kementerian Kesehatan dalam mendapatkan produk yang telah disahkan oleh BSN. Dalam membangun aplikasi ini, kami bertujuan untuk memudahkan masyarakat umum dalam mencari alat kesehatan yang resmi tanpa takut akan keaslian produk. Dengan memberikan produk yang dapat diandalkan, masyarakat akan lebih percaya dengan aplikasi kami. Aplikasi kami yang berbasis e-commerce juga memberikan nilai praktis karena masyarakat tidak perlu membeli dengan cara datang langsung ke tempat, melainkan hanya dengan membuka dan memesan melalui website. 

## Modul Aplikasi

| Modul | Person in Charge |
| --- | --- |
| Landing Page | Dimitri Theofilus Wynardo N |
| Shopping Cart | Christopher Moses Nathanael |
| Product Information Management | Brasel Isnaen Fachturrahman |
| Company Profile | Galan Gahara Triguna Toding |
| Customer Support dan Interface | Muhammad Arya Adirianto |
| User-generated Reviews | Aurora Putri Kumala Bakti |
| Account Page | Nathasya Shalsabilla Putri |

## Persona
**1. Masyarakat umum yang belum memiliki akun**

Masyarakat yang ingin mengakses aplikasi kami tetapi belum memliki akun dapat melakukan aktivitas seperti membuka halaman landing page, mencari barang, dan melihat review barang.

**2. Masyarakat umum yang sudah memiliki akun**

Saat masyarakat sudah memiliki akun, mereka dapat melakukan operasi yang sama seperti mereka yang belum memiliki akun ditambah dapat memasukkan barang yang ingin mereka beli ke dalam *shopping* cart atau keranjang. Mereka juga bisa memanfaatkan modul *Customer Support dan Interface* yaitu *chat* langsung dengan *Customer Service* ke depannya. Mereka yang sudah memiliki akun juga bisa melihat *Account Page* pribadi.

**3. Tenaga kesehatan**

Tenaga kesehatan di sini berfungsi sebagai supplier utama dari produk yang dihadirkan. Mereka akan memiliki akses seperti masyarakat umum yang belum memiliki akun. Mereka dapat mengontrol langsung jumlah barang yang tersedia. Mereka akan bekerja sama dengan developer aplikasi untuk men-*supply* produk terbaru.

